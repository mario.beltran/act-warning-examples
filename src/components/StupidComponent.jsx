import { useEffect, useState } from 'react';

const StupidComponent = () => {
  const [ctr, setCtr] = useState(0);

  useEffect(() => {
    setCtr(1);
  }, []);

  return ctr;
};

export { StupidComponent };
