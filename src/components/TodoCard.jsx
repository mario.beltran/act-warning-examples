import { useEffect, useState } from 'react';

const TodoCard = ({ id }) => {
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState(undefined);

  useEffect(() => {
    const fetchData = async () => {
      const resp = await fetch(
        `https://jsonplaceholder.typicode.com/todos/${id}`
      );

      const data = await resp.json();

      setData(data);
      setIsLoading(false);
    };

    fetchData();
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (isLoading) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <p>
        <strong>Task:</strong> {data.title}
      </p>
      <p>
        <strong>Is completed?</strong> {data.completed ? 'Yes' : 'No'}
      </p>
    </div>
  );
};

export { TodoCard };
