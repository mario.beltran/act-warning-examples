import { useEffect, useState } from 'react';

const TimeCounter = () => {
  const [count, setCount] = useState(0);

  useEffect(() => {
    const intervalId = setInterval(() => {
      setCount((prevCount) => prevCount + 1);
    }, 1000);

    return () => {
      clearInterval(intervalId);
    };
  }, []);

  return <div>Page has been open for {count} seconds.</div>;
};

export { TimeCounter };
