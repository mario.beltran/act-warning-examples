import { useState } from 'react';

const ClickCounter = () => {
  const [count, setCount] = useState(0);

  const handleClick = () => {
    setCount((prevCount) => prevCount + 1);
  };

  return (
    <div>
      <button type="button" onClick={handleClick}>
        {count}
      </button>
    </div>
  );
};

export { ClickCounter };
