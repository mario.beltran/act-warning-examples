import { useState } from 'react';

const UserCard = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState(undefined);

  const handleSubmit = (event) => {
    event.preventDefault();
    const userId = event.target.elements['user-id'].value;

    const fetchData = async () => {
      setIsLoading(true);
      const resp = await fetch(
        `https://jsonplaceholder.typicode.com/users/${userId}`
      );

      const data = await resp.json();

      await new Promise((resolve) => setTimeout(resolve, 0));

      setData(data);
      setIsLoading(false);
    };

    fetchData();
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        User id <input type="number" name="user-id" min={1} max={10} />
      </label>
      <button>Retrieve data</button>

      {isLoading && <div>Loading...</div>}
      {!isLoading && data && (
        <div>
          <p>
            <strong>Name:</strong> {data.name}
          </p>
          <p>
            <strong>Email:</strong> {data.email}
          </p>
        </div>
      )}
    </form>
  );
};

export { UserCard };
