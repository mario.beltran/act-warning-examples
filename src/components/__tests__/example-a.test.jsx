/**
 * @fileOverview Sync tests with React DOM test utils
 */
import ReactDOM from 'react-dom';
import { StupidComponent } from '../StupidComponent';
import { act } from 'react-dom/test-utils';

let container;

beforeEach(() => {
  container = document.createElement('div');
  document.body.appendChild(container);
});

afterEach(() => {
  document.body.removeChild(container);
  container = undefined;
});

test('StupidComponent NOT using act() util', () => {
  ReactDOM.render(<StupidComponent />, container);

  expect(container.innerHTML).toBe('1');
});

test('StupidComponent using act() util', () => {
  act(() => {
    ReactDOM.render(<StupidComponent />, container);
  });

  expect(container.innerHTML).toBe('1');
});
