/**
 * @fileOverview Legitimate act() usage with React Testing Library
 */
import { act, render, screen } from '@testing-library/react';
import { TimeCounter } from '../TimeCounter';

beforeAll(() => {
  jest.useFakeTimers();
});

afterAll(() => {
  jest.useRealTimers();
});

test('TimeCounter does its thing', async () => {
  render(<TimeCounter />);

  expect(
    screen.getByText(/page has been open for 0 seconds/i)
  ).toBeInTheDocument();

  act(() => jest.advanceTimersByTime(5000));

  expect(
    screen.getByText(/page has been open for 5 seconds/i)
  ).toBeInTheDocument();
});
