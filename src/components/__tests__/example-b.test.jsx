/**
 * @fileOverview Sync tests with React Testing Library
 */

import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { StupidComponent } from '../StupidComponent';
import { ClickCounter } from '../ClickCounter';

test('StupidComponent does its thing', () => {
  render(<StupidComponent />);
  expect(screen.getByText('1')).toBeInTheDocument();
});

test('ClickCounter does its thing', () => {
  render(<ClickCounter />);

  const button = screen.getByRole('button');
  expect(button).toHaveTextContent('0');

  for (let i = 0; i < 3; i++) {
    userEvent.click(button);
  }

  expect(button).toHaveTextContent('3');
});
