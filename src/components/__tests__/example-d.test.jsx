/**
 * @fileOverview Async tests (render) with React Testing Library
 */
import { act, render, screen } from '@testing-library/react';
import { TodoCard } from '../TodoCard';

beforeEach(() => {
  jest.spyOn(window, 'fetch').mockResolvedValue({
    json: jest.fn().mockResolvedValue({
      id: 99,
      userId: 5,
      title: 'Just a fake task',
      completed: false,
    }),
  });
});

test('TodoCard incorrectly', async () => {
  render(<TodoCard id={99} />);

  expect(screen.getByText('Just a fake task')).toBeInTheDocument();
});

test('TodoCard correctly', async () => {
  render(<TodoCard id={99} />);

  expect(screen.getByText('Loading...')).toBeInTheDocument();

  const todoTitle = await screen.findByText('Just a fake task');
  expect(todoTitle).toBeInTheDocument();
});
