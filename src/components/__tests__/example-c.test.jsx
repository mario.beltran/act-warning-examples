/**
 * @fileOverview Async tests (actions) with React Testing Library
 */
import { act, render, screen } from '@testing-library/react';
import { UserCard } from '../UserCard';
import userEvent from '@testing-library/user-event';

beforeEach(() => {
  jest.spyOn(window, 'fetch').mockResolvedValue({
    json: jest.fn().mockResolvedValue({
      id: 7,
      name: 'John Doe',
      email: 'john@joe.com',
    }),
  });
});

test('UserCard incorrectly - approach 1', async () => {
  await act(async () => {
    render(<UserCard />);
  });

  userEvent.type(screen.getByLabelText(/user id/i), '7');
  userEvent.click(screen.getByRole('button', { name: /retrieve data/i }));

  expect(screen.getByText('Loading...')).toBeInTheDocument();
});

test('UserCard incorrectly - approach 2', async () => {
  render(<UserCard />);

  userEvent.type(screen.getByLabelText(/user id/i), '7');
  userEvent.click(screen.getByRole('button', { name: /retrieve data/i }));

  expect(screen.getByText('John Doe')).toBeInTheDocument();
});

test('UserCard correctly', async () => {
  render(<UserCard />);

  userEvent.type(screen.getByLabelText(/user id/i), '7');
  userEvent.click(screen.getByRole('button', { name: /retrieve data/i }));

  expect(screen.getByText('Loading...')).toBeInTheDocument();

  expect(await screen.findByText('John Doe')).toBeInTheDocument();
});
