import './App.css';
import { ClickCounter } from './components/ClickCounter';
import { StupidComponent } from './components/StupidComponent';
import { TodoCard } from './components/TodoCard';
import { UserCard } from './components/UserCard';
import { TimeCounter } from './components/TimeCounter';

function App() {
  return (
    <div className="App">
      <StupidComponent />
      <hr />
      <ClickCounter />
      <hr />
      <UserCard />
      <hr />
      <TodoCard id={99} />
      <hr />
      <TimeCounter />
    </div>
  );
}

export default App;
